/*
 * AutoSMS.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package AutoSMS;

import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;

import javax.wireless.messaging.*;
import javax.microedition.io.*;
import javax.microedition.lcdui.Displayable;
import java.util.*;
import java.io.*;

import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.util.Persistable;
import net.rim.device.api.ui.Font;
import net.rim.device.api.system.Bitmap;
import javax.microedition.pim.*;
import net.rim.device.api.util.StringUtilities;

/**
 * 
 */
class AutoSMS extends UiApplication{
    
    private static String _openString = "sms://:0"; // See Connector implementation notes.
    private boolean _messconnok = false;
    private MessageConnection messconn;
    Vector _smsList = new Vector();
    private ListField _messageListField;
    private PersistentObject _persistsms;
    
    private Font _listfontbold;
    private Font _listfontnormal;
    
    private ReceiveThread _receivethread;
    private UserSettingsScreen _userSettingsscreen = new UserSettingsScreen();
    private int _lastsentmessageid = 2;
    private boolean _needtoreply = false;
    private String _replyaddress = "";
    private ContactListScreen _contactlistscreen = new ContactListScreen();
            
    Bitmap _crossbitmap;
    Bitmap _openedbitmap;
    Bitmap _sendingbitmap;
    Bitmap _tickbitmap;
    Bitmap _unreadbitmap;
    
    private MenuItem _optionsMenuItem = new MenuItem("Options", 100, 10) 
    {                
        public void run()
        {
            _userSettingsscreen = new UserSettingsScreen();            
            UiApplication.getUiApplication().pushModalScreen(_userSettingsscreen);           
        }
    };
    
    private MenuItem _sendMenuItem = new MenuItem("Send SMS", 100, 10) 
    {                
        public void run()
        {
            //addReceivedMessage("sms://07710978853", "This is a received message");
        }
    };
    
    private MenuItem _shutdownMenuItem = new MenuItem("Shutdown", 100, 10) 
    {                
        public void run()
        {
            //addReceivedMessage("sms://07710978853", "This is a received message");
            System.exit(0);
        }
    };            
    
    private MenuItem _deleteMenuItem = new MenuItem("Delete", 100, 10) 
    {                
        public void run()
        {
            
            if (Dialog.ask(Dialog.D_YES_NO, "Delete message?") == Dialog.YES) {
                int index = _messageListField.getSelectedIndex();
                
                _smsList.removeElementAt(index);
                
                _persistsms.commit();
                
                _messageListField.setSize(_smsList.size());
            }                                   
        }
    };
    
    private MenuItem _aboutMenuItem = new MenuItem("About", 100, 10) 
    {                
        public void run()
        {
            Dialog.alert("AutoSMS v1.3\n\nCopyright (c) 2008-2009 CedeSoft Ltd.\n\nAll Rights Reserved.");
        }
    };
    
    public static void main(String[] args)
    {
        
        // Create a new instance of the application and start 
        // the application on the event thread.
        AutoSMS sms  = new AutoSMS();
        sms.enterEventDispatcher();
    }
    
    AutoSMS() {    
        
        _listfontbold = Font.getDefault();
        _listfontbold = _listfontbold.derive(Font.BOLD);
        _listfontnormal = Font.getDefault();               
       
        _crossbitmap = Bitmap.getBitmapResource("Cross.png");
        _openedbitmap = Bitmap.getBitmapResource("Opened.png");
        _sendingbitmap = Bitmap.getBitmapResource("Sending.png");
        _tickbitmap = Bitmap.getBitmapResource("Tick.png");
        _unreadbitmap = Bitmap.getBitmapResource("Unread.png");        
       
        try {           
            // Get our receiving port connection.
            messconn = (MessageConnection) Connector.open("sms://:0");
            _messconnok = true;
        } catch (IOException e) {
            // Handle startup errors
            _messconnok = false;
        }                
        
        AutoSMSMainScreen screen = new AutoSMSMainScreen();
        pushScreen(screen);
    }
    
    public String resolveContactname (String strNumber)
    {
        _contactlistscreen = new ContactListScreen();
        
        if (_contactlistscreen.doesMobileexist(strNumber) == true) {
            return _contactlistscreen.resolveName(strNumber);
        } else {
            return strNumber;
        } 
    }
    
    public void loadSMSlist()
    {
            //0c5ef3fc57ea85dd64e25af80642b0a0 //AutoSMS
            long KEY =  0x5dd64e25af80642bL;
            _persistsms = PersistentStore.getPersistentObject( KEY );
            _smsList = (Vector) _persistsms.getContents();
            if( _smsList == null ) {
                _smsList = new Vector();
                AddSMSItem(new SmsMessage("01234123456","Sample Message"));   
                _persistsms.setContents( _smsList );
                //persist.commit()
            }
    }
    
    public void ReverseList ()
    {
        Vector _backupList = new Vector();
        SmsMessage smsmessage;                
            
        int a = 0;

        
        for (a=0;a<_smsList.size();a++) {
            smsmessage = new SmsMessage();
            smsmessage = (SmsMessage) _smsList.elementAt(_smsList.size()-a-1);
            _backupList.addElement(smsmessage);
        }
        
        _smsList.removeAllElements();
        
        for (a=0;a<_backupList.size();a++) {
            smsmessage = new SmsMessage();
            smsmessage = (SmsMessage) _backupList.elementAt(a);
            _smsList.addElement(smsmessage);
        }

        
        _backupList.removeAllElements();
    }
    
    public void AddSMSItem (SmsMessage smsmsg)
    {
        ReverseList();
        _smsList.addElement(smsmsg);
        ReverseList();
        _persistsms.commit();
    }
    
    
    // ================================================================ MAIN SCREEN
    // ============================================================================
    // ============================================================================
    // ============================================================================
    // ============================================================================
    
    
        // CedeSMS Main screen - showing a list of current received messages.
    private class AutoSMSMainScreen extends MainScreen implements ListFieldCallback, MessageListener {

    //{
        
        // Constructor
        private AutoSMSMainScreen()
        {
            setTitle(new LabelField("AutoSMS  - Messages", LabelField.USE_ALL_WIDTH));
            
            // Register a listener for inbound messages.
            try {
                if (_messconnok == true) {
                    messconn.setMessageListener(this);
                }
            } catch (IOException e) {
                
            }            
                                       
            _messageListField = new ListField();
            _messageListField.setRowHeight ((_listfontnormal.getHeight()*2)+1);
            
            _messageListField.setCallback( this );
            
            add(_messageListField);
            addMenuItem(_deleteMenuItem);
            addMenuItem(_optionsMenuItem);
            addMenuItem(_shutdownMenuItem);
            addMenuItem(_aboutMenuItem);
           
            // Add some sample elements to our vector array
            //_smsList.addElement(new SmsMessage("01234123456","Sample Message"));
            loadSMSlist(); 
            
            if (_messconnok == false) {
                //AddSMSItem(new SmsMessage("00000000000","CedeSMS was unable to listen for incoming SMS messages. This may be because a previous version of CedeSMS is running. Please remove any previous versions of CedeSMS."));   
            }
                    
            _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.   
            
            RegCheck();                     
        }
         
         
        public void RegCheck()
        {     
                UiApplication.getUiApplication().invokeLater( new Runnable()
                {
                    public void run ()
                    {
                        
                        _userSettingsscreen = new UserSettingsScreen();
                        UserSettings mySettings = _userSettingsscreen.getUserSettings();
                        
                        if (mySettings._registered == false) {
                            
                            
                            UiApplication.getUiApplication().pushModalScreen(new PasswordScreen());
                            
                            
                            //if (_regscreen.isPasswordok() == true) {
                                //setRegistered(true);
                                _userSettingsscreen.setRegistered(true);
                            //}
                        }
    
                    }
                });
        }
            
              
        public void notifyIncomingMessage(MessageConnection conn) {
            if (conn == messconn) {
                //reader.handleMessage();               
                _receivethread = new ReceiveThread ();
                _receivethread.start();
            }
        }
        
                
        /**
         * Prevent the save dialog from being displayed.
         * 
         * @see net.rim.device.api.ui.container.MainScreen#onSavePrompt()
         */
        public boolean onSavePrompt()
        {
            return true;
        }    

        
        /**
        * Close application
        * 
        * @see net.rim.device.api.ui.Screen#close() 
        */
        public void close()
        {
            //_listener.stop();
            //_sender.stop();
            
            //super.close();
            Dialog.alert("Press the Red Hangup button to get back to the Blackberry home screen.");            
        }
                
        private SmsMessage getSelectedSMS() 
        {
            int selectedIndex = _messageListField.getSelectedIndex();
            
            if ( selectedIndex == -1 ) 
            {
                return null;
            }
            
            SmsMessage smsmessage = (SmsMessage) _smsList.elementAt( selectedIndex );
            smsmessage.setUnread (false);
            _smsList.setElementAt(smsmessage, selectedIndex); 
            _persistsms.commit();
            
            return (SmsMessage) _smsList.elementAt( selectedIndex );
        }    
                
        
        public boolean invokeAction(int action)
        {        
            switch(action)
            {
                case ACTION_INVOKE: // Trackball click.
                    SmsMessage smsmessage = getSelectedSMS();
                    
                    if (smsmessage != null) {
                
                    }
                    return true; // We've consumed the event.
            }   
            
            return  super.invokeAction(action);
        }
        
        //////////////////////////////////////
        // ListFieldCallback methods
        //////////////////////////////////////    
        
        /**
        * Draws a row in the list of memos.
        * 
        * @param listField The ListField whose row is being drawn.
        * @param graphics The graphics context to use for drawing.
        * @param index The index of the row being drawn.
        * @param y The distance from the top of the screen where the row is being drawn.
        * @param width The width of the row being drawn.
        * 
        * @see net.rim.device.api.ui.component.ListFieldCallback#drawListRow(ListField,Graphics,int,int,int)
        */
        public void drawListRow( ListField listField, Graphics graphics, int index, int y, int width ) 
        {
            SmsMessage smsmessage = (SmsMessage) get( listField, index );
            
            if (smsmessage.getUnread() == true) {
                graphics.setFont(_listfontbold);
            } else {
                graphics.setFont(_listfontnormal);
            }
           
            
            if (smsmessage.getType () == 0) {
                graphics.drawText("From: " + smsmessage.getAddress (), 44, y, 0, width-44 );                                              
            } else {
                graphics.drawText("To: " + smsmessage.getAddress (), 44, y, 0, width-44 );
            }                        
            
            graphics.setColor(Color.GRAY);            
            graphics.drawText (smsmessage.getMessage(), 44, y+_listfontnormal.getHeight(), 0, width-44);                                    
            
            if (smsmessage.getType () == 0) {
                if (smsmessage.getUnread() == true) {
                    graphics.drawBitmap(2, y+3, 40, 33, _unreadbitmap, 0, 0);
                } else {
                    graphics.drawBitmap(2, y+3, 40, 33, _openedbitmap, 0, 0);
                }
            } else {
                if (smsmessage.getSendstatus() == 0) {
                    graphics.drawBitmap(2, y+3, 40, 33, _sendingbitmap, 0, 0);
                } 
                
                if (smsmessage.getSendstatus() == 1) {
                    graphics.drawBitmap(2, y+3, 40, 33, _tickbitmap, 0, 0);
                } 
                
                if (smsmessage.getSendstatus() == 2) {
                    graphics.drawBitmap(2, y+3, 40, 33, _crossbitmap, 0, 0);
                }               
                
            } 
            
            graphics.drawLine (0, y+(_listfontnormal.getHeight()*2), width, y+(_listfontnormal.getHeight()*2));
        }    
        
        /**
        * Retrieves the element from the specified ListField at the specified index. 
        * @param listField The ListField from which to retrieve the element.
        * @param index The index into the ListField from which to retrieve the element.
        * @return The requested element.
        * 
        * @see net.rim.device.api.ui.component.ListFieldCallback#get(ListField , int)
        */
        public Object get( ListField listField, int index ) 
        {
            return _smsList.elementAt( index );
        }    
        
        /**
        * Returns the preferred width of the provided ListField. 
        * @param listField The ListField whose preferred width is being retrieved.
        * @return The ListField's preferred width.
        * 
        * @see net.rim.device.api.ui.component.ListFieldCallback#getPreferredWidth(ListField)
        */
        public int getPreferredWidth( ListField listField ) 
        {
            //return Display.getWidth();
            return getWidth();
        }    
        
        /**
        * Retrieves the first occurrence of the provided prefix in the list (not implemented). 
        * @param listField The ListField being searched.
        * @param prefix The prefix to search for.
        * @param start List item at which to start the search.
        * @return -1 (not implemented).
        * 
        * @see net.rim.device.api.ui.component.ListFieldCallback#indexOfList(ListField,String,int)
        */
        public int indexOfList( ListField listField, String prefix, int start ) 
        {
            return -1;
        }
        
    }

    // ================================================================ RECEIVING STUFF
    // ============================================================================
    // ============================================================================
    // ============================================================================
    // ============================================================================
    
    
    public String CleanSMSAddress (String address)
    {
        String strFirstpart = "";
        int iColonindex = 0;
        
        if (address.length() > 6) {
            strFirstpart = address.substring(6);
        } else {
            strFirstpart = address;
        }
        //iColonindex = strFirstpart.indexOf(":");
        return strFirstpart;
    }
    
    private void addReceivedMessage(final String address, final String msg)
    {
        invokeLater(new Runnable() 
        {           
            public void run()
            {
                String strcleanaddress = CleanSMSAddress(address);
                AddSMSItem(new SmsMessage (strcleanaddress, msg, true));                
                _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
                
                // Now check if reply is turned on, and reply to this message
                _userSettingsscreen = new UserSettingsScreen();
                UserSettings mySettings = _userSettingsscreen.getUserSettings();
                
                if (mySettings._usereply.compareTo("yes") == 0) {                    
                    //SendSMS(strcleanaddress, mySettings._autoreplymessage);                    
                    _needtoreply = true;
                    _replyaddress = strcleanaddress;
                }
                
                if (mySettings._useforward.compareTo("yes") == 0) {
                    SendSMS(mySettings._autoforwardnumber, "FW From: " + resolveContactname(strcleanaddress) + " - " + msg);
                }
            }
        });
    }
    
    private void receivedSmsMessage(Message m)
    {
        String address = m.getAddress();
        String msg = null;
        
        if ( m instanceof TextMessage )
        {
            TextMessage tm = (TextMessage) m;
            msg = tm.getPayloadText();
        }
        
        addReceivedMessage (address, msg);
    }
    
    private class ReceiveThread extends Thread
    {
        
        public void run ()
        {
            try 
            {                                                                                   
                Message m = messconn.receive();
                receivedSmsMessage(m);                
            } 
            catch (IOException e)
            {
                // Likely the stream was closed.
                System.err.println(e.toString());
            }            
        }
    }
    
    // ================================================================ SENDING STUFF
    // ============================================================================
    // ============================================================================
    // ============================================================================
    // ============================================================================
    public void SendSMS (String addr, String msg)
    {
            SendThreadEx newthread = new SendThreadEx (addr, msg);
            newthread.start();
    }
    
    private class SendThreadEx extends Thread
    {
        //private volatile boolean _start = true;
        private String _addr = "";
        private String _msg = "";
        
        public SendThreadEx (String addr, String msg)
        {
            _addr = addr;
            _msg = msg;
        }
        
        public void run ()
        {
            //addSamplemessage(_addr, _msg);
            boolean success = false;
            try {
                
                String addr = "sms://" + _addr + ":0";
                MessageConnection conn = (MessageConnection) Connector.open(addr);
                TextMessage tmsg = (TextMessage)conn.newMessage(MessageConnection.TEXT_MESSAGE);
                //msg.setPayloadText("Hello World!");
                tmsg.setPayloadText(_msg);
                addSentMessage(_addr, _msg);
                try 
                {
                    sleep(2000);
                } 
                catch (InterruptedException e) 
                {
                    //System.err.println(e.toString());
                }
                conn.send(tmsg);
                updateOutgoingmessagestatus(1);
                success = true;
            } catch (Exception e) {
                success = false;          
                updateOutgoingmessagestatus(2);      
            }
        }
    }
        
    private void addSentMessage(final String address, final String msg)
    {
        invokeLater(new Runnable() 
        {
                       
            public void run()
            {
                
                /*
                 private int _type = 0; // 0 = incoming, 1 = outgoing
                private int _sendstatus = 0; // 0 = sending, 1 = success, 2 = failure
                private int _ID = 1; // record ID for updating status in the list
                */
                
                AddSMSItem(new SmsMessage (address,msg, false, 1, 0, _lastsentmessageid));                
                _lastsentmessageid++;
                //_smsList.addElement(new SmsMessage(CleanSMSAddress(address),msg, true));
                _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
            }
        });
    }
    
    private void updateOutgoingmessagestatus (final int status)
    {
        invokeLater(new Runnable() 
        {
                       
            public void run()
            {
                SmsMessage msg;
                
                /*
                 private int _type = 0; // 0 = incoming, 1 = outgoing
                private int _sendstatus = 0; // 0 = sending, 1 = success, 2 = failure
                private int _ID = 1; // record ID for updating status in the list
                */
                                
                int a = 0;
                for (a=0;a<_smsList.size();a++) {
                    msg = (SmsMessage) _smsList.elementAt(a);
                    
                    if (msg.getID() == _lastsentmessageid-1) {
                        msg.setSendstatus(status);
                        _smsList.setElementAt(msg, a);
                    }
                    
                }
                
                _messageListField.setSize(_smsList.size() );  // Causes the list to be updated and painted.
                
                
                if (_needtoreply == true) {
                    _needtoreply = false;
                    
                    _userSettingsscreen = new UserSettingsScreen();
                    UserSettings mySettings = _userSettingsscreen.getUserSettings();
                    
                    if (mySettings._usereply.compareTo("yes") == 0) {                    
                        SendSMS(_replyaddress, mySettings._autoreplymessage);                    
                    }
                }                                
            }
        });
    }
       
    
    private static final class SmsMessage implements Persistable
    {
        private String _address;
        private String _msg;
        private boolean _unread = false;
        private int _type = 0; // 0 = incoming, 1 = outgoing
        private int _sendstatus = 0; // 0 = sending, 1 = success, 2 = failure
        private int _ID = 1; // record ID for updating status in the list

        private SmsMessage()
        {
            _address = "";
            _msg = "";
        }        

        private SmsMessage(String address, String msg)
        {
            _address = address;
            _msg = msg;
        }
        
        private SmsMessage(String address, String msg, boolean bUnread)
        {
            _address = address;
            _msg = msg;
            _unread = bUnread;
        }
               
        public SmsMessage (String address, String msg, boolean bUnread, int type, int sendstatus, int ID)
        {
            _address = address;
            _msg = msg;
            _unread = bUnread;
            _type = type;
            _sendstatus = sendstatus;
            _ID = ID;
        }
        
        public int getType ()
        {
            return _type;
        }
        
        public void setType (int type)
        {
            _type = type;
        }
        
        public int getSendstatus ()
        {
            return _sendstatus;
        }
        
        public void setSendstatus (int sendstatus)
        {
            _sendstatus = sendstatus;
        }
        
        public int getID ()
        {
            return _ID;
        }
        
        public void setID (int ID)
        {
            _ID = ID;
        }
        
        public String getAddress ()
        {
            return _address;
        }
        
        public String getMessage ()
        {
            return _msg;
        }
        
        public boolean getUnread ()
        {
            return _unread;
        }
        
        public void setUnread (boolean bUnread)
        {
            _unread = bUnread;
        }
        
        private Message toMessage(MessageConnection mc)
        {
            TextMessage m = (TextMessage) mc.newMessage(MessageConnection.TEXT_MESSAGE , "//" + _address + ":0");
            m.setPayloadText(_msg);
            
            return m;
        }
    }
} 
