/*
 * UserSettings.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package AutoSMS;

import net.rim.device.api.util.Persistable;


/**
 * 
 */
class UserSettings implements Persistable {
    
    public String _usereply = "no";
    public String _useforward = "no";
    public String _autoreplymessage = "";
    public String _autoforwardnumber = "";
    public boolean _registered = false;
    
    UserSettings() {    
               
    }
    
}
