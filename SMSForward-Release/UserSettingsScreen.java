/*
 * UserSettingsScreen.java
 *
 * � <your company here>, 2003-2008
 * Confidential and proprietary.
 */

package AutoSMS;

import java.util.*;
import net.rim.device.api.ui.*;
import net.rim.device.api.ui.component.*;
import net.rim.device.api.ui.container.*;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.system.PersistentObject;


class UserSettingsScreen extends MainScreen {
    
    private ObjectChoiceField _efuseforward;
    private ObjectChoiceField _efusereply;
        
    private EditField _efautoforwardnumber;
    private EditField _efautoreplymessage;
    
    private UserSettings _settings = new UserSettings();    
    private PersistentObject _persist;
    
    String[] stryesno = new String[2];
    
    private MenuItem _saveMenuItem = new MenuItem("Save", 100, 10) 
    {
        public void run()
        {
            
            
            //Dialog.alert("Contact Saved!" + dtCurrent.toString());
            //setLocalcontact();
            
            setLocalsettings();
            _persist.commit();
            
            
            UiApplication uiapp = UiApplication.getUiApplication();
            uiapp.popScreen(uiapp.getActiveScreen());
            
        }
    };
     
    UserSettingsScreen() {    
        setTitle(new LabelField("AutoSMS Options" , LabelField.ELLIPSIS | LabelField.USE_ALL_WIDTH));
        
        //_fromaddress = new EditField("From:", "", MAX_PHONE_NUMBER_LENGTH, EditField.FILTER_PHONE);       
        _efautoforwardnumber = new EditField("Auto Forward Number: ", "");
        _efautoreplymessage = new EditField("Auto Reply Message: ", "");
        
                        
        stryesno[0] = "yes";
        stryesno[1] = "no";
        
        _efuseforward = new ObjectChoiceField("Use Auto Forward: ", stryesno);
        _efusereply = new ObjectChoiceField("Use Auto Reply: ", stryesno);
        
        
        add(_efuseforward);
        add(_efusereply);
        add(new SeparatorField());
        add(_efautoforwardnumber);
        add(_efautoreplymessage);
                
        addMenuItem(_saveMenuItem);
        
        loadSettings();
        setLocalform();
        
    }
    
    public boolean onSave()
    {
        //Dialog.alert("Auto Saved!");
        //setLocalcontact();
        //_recordsaved = true;        
        setLocalsettings();
        _persist.commit();
        return true;
    }
    
    
    public void loadSettings ()
    {
         //0c5ef3fc57ea85dd64e25af80642b0a0 //AutoSMS
         long KEY =  0xef3fc57ea85dd64eL;
         _persist = PersistentStore.getPersistentObject( KEY );
         _settings = (UserSettings) _persist.getContents();
         if( _settings == null ) {
             _settings = new UserSettings();
             _persist.setContents( _settings );
             //persist.commit()
         }
    }
        
    public void setLocalsettings()
    {
        _settings._autoforwardnumber = _efautoforwardnumber.getText();
        _settings._autoreplymessage = _efautoreplymessage.getText();
        _settings._useforward = getMultichoice(_efuseforward, stryesno);
        _settings._usereply = getMultichoice(_efusereply, stryesno);
    }
    
    public void setLocalform()
    {
        _efautoforwardnumber.setText(_settings._autoforwardnumber);
        _efautoreplymessage.setText(_settings._autoreplymessage);
        setMultichoice(_efuseforward, _settings._useforward, stryesno);
        setMultichoice(_efusereply, _settings._usereply, stryesno);       
    }
        
    public boolean isRegistered()
    {
        return _settings._registered;
    }
    
    public void setRegistered(boolean registered)
    {
        _settings._registered = registered;
        _persist.commit();
    } 
       
    public void setMultichoice(ObjectChoiceField myfield, String strChoice, String[] mychoices)
    {
        if (strChoice.trim() == "") {
            myfield.setSelectedIndex(0);
        } else {
            
            int c = 0;
            String strcur = "";
            
            for(c=0;c<mychoices.length;c++)
            {
                strcur = mychoices[c];
                if (strcur.compareTo(strChoice) == 0) {
                    myfield.setSelectedIndex(c);
                    return;
                }
            }            
        }
    }
    
    public String getMultichoice (ObjectChoiceField myfield, String[] mychoices)
    {
        int index = myfield.getSelectedIndex();
        
        return mychoices[index];
    }
    
    
    public UserSettings getUserSettings()
    {
        return _settings;
    }
    
    public void setUserSettings (UserSettings settings)
    {
        _settings = settings;
        _persist.commit();
    }
    
    public void clearSettings ()
    {               
        _settings._autoforwardnumber = "";
        _settings._autoreplymessage = "";
        _settings._useforward = "no";
        _settings._usereply = "no";
        
        _persist.commit();
    }
}
 
